import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import { RootState } from "../redux/store";
import { UserObject } from "../types";

const PrivateRoute: React.FC<{
  component: React.FC;
  path: string;
  exact: boolean;
}> = (props) => {
  const user: UserObject = useSelector((state: RootState) => state.user);

  return user?.data ? (
    <Route path={props.path} exact={props.exact} component={props.component} />
  ) : (
    <Redirect to={`/`} />
  );
};

export default PrivateRoute;
