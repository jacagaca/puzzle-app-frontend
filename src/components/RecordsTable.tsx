import { AnimatePresence, motion } from "framer-motion";
import moment from "moment";
import React from "react";
import { Loader, Table } from "semantic-ui-react";

interface IRecord {
  createdAt: string;
  updatedAt: string;
  username: string;
  record: number;
  __v: number;
  _id: string;
}

const RecordsTable = ({ recordsList }: any) => {
  if (recordsList.length < 2)
    return (
      <Loader inverted size="huge">
        Loading
      </Loader>
    );

  return (
    <AnimatePresence>
      <motion.div
        style={{ margin: 0, padding: 0 }}
        key={"recordsTable"}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 0.8 }}
      >
        <Table celled>
          <Table.Header>
            <Table.Row textAlign="center">
              <Table.HeaderCell>Username</Table.HeaderCell>
              <Table.HeaderCell>Score</Table.HeaderCell>
              <Table.HeaderCell>Date</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {recordsList
              .filter((item: IRecord) => item.record > 0)
              .sort(
                (a: IRecord, b: IRecord) =>
                  b.record > 0 && a.record > 0 && b.record - a.record
              )
              .slice(0, 5)
              .map((item: IRecord, index: number) => {
                return (
                  <Table.Row textAlign="center" key={index}>
                    <Table.Cell>{item.username}</Table.Cell>
                    <Table.Cell>{item.record}</Table.Cell>
                    <Table.Cell>
                      {moment(item.createdAt).format("YYYY-MM-DD, h:mm:ss a")}
                    </Table.Cell>
                  </Table.Row>
                );
              })}
          </Table.Body>
        </Table>
      </motion.div>
    </AnimatePresence>
  );
};

export default RecordsTable;
