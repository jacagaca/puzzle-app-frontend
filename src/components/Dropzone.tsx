import React from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import { useMediaQuery } from "react-responsive";
import { smallScreen } from "../custom/utils";

interface DropzoneProps {
  position: any;
  id: string;
}

interface PositionProps {
  name: string;
  img: string;
  id: string;
}
interface LetterProps {
  name: string;
  img: string;
  id: string;
  index: number;
}

const getListStyle = (isDraggingOver: boolean) => ({
  display: "flex",
  padding: 20,
  margin: "auto",
  justifyContent: "left",
  height: "140px",
  width: "772px",
});

const getListStyleSmallScreen = (isDraggingOver: boolean) => ({
  width: 85,
});

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
  userSelect: "none",
  border: isDragging ? "solid 5px lightgreen" : "solid 5px white",
  backgroundColor: "white",
  borderRadius: "5px",
  marginLeft: "18px",
  marginRight: "18px",
  zIndex: 2,
  ...draggableStyle,
});

const getItemStyleSmallScreen = (isDragging: boolean, draggableStyle: any) => ({
  userSelect: "none",
  border: isDragging ? "solid 5px lightgreen" : "solid 5px white",
  backgroundColor: "white",
  borderRadius: "5px",
  zIndex: 2,
  marginBottom: "4px",
  ...draggableStyle,
});

const Dropzone = ({ position, id }: DropzoneProps) => {
  const isSmallScreen = useMediaQuery({ query: smallScreen });

  return (
    <Droppable
      droppableId={id}
      direction={isSmallScreen ? "vertical" : "horizontal"}
    >
      {(provided, snapshot) => {
        return (
          <div
            style={
              isSmallScreen
                ? getListStyleSmallScreen(snapshot.isDraggingOver)
                : getListStyle(snapshot.isDraggingOver)
            }
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {position.map(({ name, img, id }: PositionProps, index: number) => (
              <Letter id={id} key={name} name={name} img={img} index={index} />
            ))}

            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
  );
};

const Letter = ({ name, index, id, img }: LetterProps) => {
  const isSmallScreen = useMediaQuery({ query: smallScreen });

  return (
    <Draggable key={name} draggableId={name} index={index}>
      {(provided, snapshot) => {
        return (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={
              isSmallScreen
                ? getItemStyleSmallScreen(
                    snapshot.isDragging,
                    provided.draggableProps.style
                  )
                : getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style
                  )
            }
          >
            <img
              className="puzzle"
              {...provided.dragHandleProps}
              {...provided.draggableProps}
              ref={provided.innerRef}
              style={{ padding: "15px" }}
              src={img}
              alt="zoovu-letter"
            ></img>
          </div>
        );
      }}
    </Draggable>
  );
};

export default Dropzone;
