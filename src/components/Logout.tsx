import React from "react";
import { useDispatch } from "react-redux";
import history from "../custom/history";
import { logoutSuccess } from "../redux/actions";
import { smallScreen } from "../custom/utils";
import { useMediaQuery } from "react-responsive";

interface Props {
  handleRestartGame: any;
}

const Logout: React.FC<Props> = ({ handleRestartGame }: Props) => {
  const dispatch = useDispatch();
  const isSmallScreen = useMediaQuery({ query: smallScreen });

  const handleLogout = async () => {
    try {
      window.localStorage.removeItem("token");
      history.push(`/`);
      dispatch(logoutSuccess());
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={isSmallScreen ? "ui buttons" : "huge ui buttons"}>
      <button className="ui button" onClick={handleLogout}>
        <i className="frown outline icon" />
        Logout
      </button>
      <div className="or"></div>
      <button className="ui positive button" onClick={handleRestartGame}>
        <i className="redo icon white" />
        Shuffle
      </button>
    </div>
  );
};

export default Logout;
