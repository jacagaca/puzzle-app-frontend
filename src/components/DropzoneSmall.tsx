import React, { useEffect } from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import { useMediaQuery } from "react-responsive";
import { penalty } from "../custom/data";
import { DropzoneSmallProps, LetterProps } from "../custom/interfaces";
import { smallScreen } from "../custom/utils";
import { EmptyContainer } from "../styles/EmptyContainer";

const getListStyleSmallScreen = () => ({
  width: 80,
  height: 80,
  padding: 0,
  marginBottom: "4px",
});

const getListStyle = () => ({
  display: "flex",
  padding: 20,
  width: "147px",
  height: "140px",
});

const getItemStyleSmallScreen = (isDragging: boolean, draggableStyle: any) => ({
  userSelect: "none",
  border: isDragging ? "solid 5px lightgreen" : "solid 5px white",
  backgroundColor: "white",
  borderRadius: "5px",
  marginBottom: "5px",
  zIndex: 2,
  ...draggableStyle,
});

const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
  userSelect: "none",
  border: isDragging ? "solid 5px lightgreen" : "solid 5px white",
  backgroundColor: "white",
  borderRadius: "5px",
  marginLeft: "18px",
  marginRight: "18px",
  zIndex: 2,
  ...draggableStyle,
});

const DropzoneSmall = ({
  position,
  id,
  handleSetPenalty,
  letter,
}: DropzoneSmallProps) => {
  const isSmallScreen = useMediaQuery({ query: smallScreen });

  useEffect(() => {
    if (position[0]) {
      const placedLetter = position[0].letter;

      if (placedLetter !== letter) {
        handleSetPenalty(penalty);
      }
    }
  }, [position]);

  return (
    <Droppable
      droppableId={id}
      direction={isSmallScreen ? "vertical" : "horizontal"}
      isDropDisabled={position[0]?.name ? true : false}
    >
      {(provided) => {
        return (
          <div
            style={isSmallScreen ? getListStyleSmallScreen() : getListStyle()}
            {...provided.droppableProps}
            ref={provided.innerRef}
          >
            {position && position[0] && (
              <Letter
                id={id}
                key={position[0].key}
                name={position[0].name}
                img={position[0].img}
                index={position[0].key}
              />
            )}

            <EmptyContainer key={1}></EmptyContainer>

            {provided.placeholder}
          </div>
        );
      }}
    </Droppable>
  );
};

const Letter = ({ name, img }: LetterProps) => {
  const isSmallScreen = useMediaQuery({ query: smallScreen });

  return (
    <Draggable key={name} draggableId={name} index={1}>
      {(provided, snapshot) => {
        return (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={
              isSmallScreen
                ? getItemStyleSmallScreen(
                    snapshot.isDragging,
                    provided.draggableProps.style
                  )
                : getItemStyle(
                    snapshot.isDragging,
                    provided.draggableProps.style
                  )
            }
          >
            <img
              className="puzzle"
              {...provided.dragHandleProps}
              {...provided.draggableProps}
              ref={provided.innerRef}
              style={{ padding: "15px" }}
              src={img}
              alt="zoovu-letter"
            ></img>
          </div>
        );
      }}
    </Draggable>
  );
};

export default DropzoneSmall;
