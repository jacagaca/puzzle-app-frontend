import React, { useEffect, useState } from "react";
import FadeIn from "react-fade-in";
import { Button, Dropdown, Header, Modal } from "semantic-ui-react";
import {
  allTimeDate,
  dayBeforeDate,
  todayDate,
  weekBeforeDate,
  yearBeforeDate,
} from "../custom/dates";
import { GetRecords } from "../services/record";
import RecordsTable from "./RecordsTable";

interface ModalProps {
  open: boolean;
  handleOpenModal: any;
  handleCloseModal: any;
  handleRestartGame: any;
  score: number;
  isRunning: boolean;
}

const initialRecord = {
  createdAt: "",
  updatedAt: "",
  username: "",
  record: "",
  __v: "",
  _id: "",
};

const initialState = {
  allTime: [initialRecord],
  today: [initialRecord],
  week: [initialRecord],
  year: [initialRecord],
};

const GameModal: any = ({
  open,
  handleOpenModal,
  handleCloseModal,
  handleRestartGame,
  score,
}: ModalProps) => {
  const [counter, setCounter] = useState(10);
  const [records, setRecords] = useState(initialState);
  const [recordsList, setRecordsList] = useState(records.today);
  const [title, setTitle] = useState("24 Hours");

  const handleChangeSortingAllTime = () => {
    setRecordsList(records.allTime);
    setTitle("All Time");
  };
  const handleChangeSortingToday = () => {
    setRecordsList(records.today);
    setTitle("24 Hours");
  };
  const handleChangeSortingWeek = () => {
    setRecordsList(records.week);
    setTitle("Week");
  };
  const handleChangeSortingYear = () => {
    setRecordsList(records.year);
    setTitle("Year");
  };

  useEffect(() => {
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);

    if (counter <= 0) {
      handleRestartGame();
    }

    return () => clearTimeout(counter);
  }, [counter]);

  async function getRecords() {
    try {
      const todayRecords = await GetRecords(dayBeforeDate, todayDate);
      const weekRecords = await GetRecords(weekBeforeDate, todayDate);
      const yearRecords = await GetRecords(yearBeforeDate, todayDate);
      const allTimeRecords = await GetRecords(allTimeDate, todayDate);

      setRecords({
        today: todayRecords,
        week: weekRecords,
        year: yearRecords,
        allTime: allTimeRecords,
      });

      setRecordsList(todayRecords);
    } catch (err) {
      console.log(err);
    }
  }

  useEffect(() => {
    if (counter) {
      getRecords();
    }
  }, [counter]);

  return (
    <Modal
      onClose={handleCloseModal}
      onOpen={handleOpenModal}
      open={open}
      size="small"
      className="modal"
    >
      <Modal.Header style={{ textAlign: "center", fontSize: "30px" }}>
        <FadeIn>Game has ended! </FadeIn>
      </Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <FadeIn>
            <Header style={{ textAlign: "center", fontSize: "18px" }}>
              <p style={{ fontSize: "20px" }}>
                Your time is:{" "}
                <span style={{ color: "#21ba45" }}>
                  {score < 0 ? 0 : score} second
                  {score === 1 ? "" : "s"}!
                </span>
              </p>
              <span>
                Best players of:{" "}
                <Dropdown
                  style={{
                    fontSize: "16px",
                    color: "#21ba45",
                    border: "solid 3px",
                    padding: "6px",
                    borderRadius: "20px",
                  }}
                  text={title}
                >
                  <Dropdown.Menu>
                    <Dropdown.Item
                      text="24 Hours"
                      description="24 Hours best records"
                      onClick={handleChangeSortingToday}
                    />
                    <Dropdown.Item
                      text="Week"
                      onClick={handleChangeSortingWeek}
                      description="Week best records"
                    />
                    <Dropdown.Item
                      text="Year"
                      onClick={handleChangeSortingYear}
                      description="Year best records"
                    />
                    <Dropdown.Item
                      text="All Time"
                      description="All time best records"
                      value={"AllTime"}
                      onClick={handleChangeSortingAllTime}
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </span>

              <RecordsTable
                recordsList={recordsList}
                style={{ marginTop: "10px" }}
              ></RecordsTable>
              <p style={{ paddingTop: "10px", fontSize: "15px" }}>
                Game will restart in {counter} second{counter < 2 ? "" : "s"}
              </p>
            </Header>
          </FadeIn>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions style={{ textAlign: "center" }}>
        <FadeIn className="modal-buttons">
          <Button
            style={{ marginLeft: "20px" }}
            size="massive"
            content="Restart the game!"
            labelPosition="right"
            icon="game"
            onClick={handleRestartGame}
            color="green"
          />
        </FadeIn>
      </Modal.Actions>
    </Modal>
  );
};

export default GameModal;
