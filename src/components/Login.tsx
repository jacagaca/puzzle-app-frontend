/* eslint-disable jsx-a11y/anchor-is-valid */
import { Field, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";
import history from "../custom/history";
import { loginSuccess, registerSuccess } from "../redux/actions";
import { RootState } from "../redux/store";
import { LoginWithToken, RegisterWithToken } from "../services/auth";
import { LoginStyles } from "../styles/LoginStyle";
import { UserObject } from "../types";

const loadingTrue = {
  error: false,
  status: 200,
  message: "Loading",
  loading: true,
  token: "",
};

const loadingFalse = {
  error: false,
  status: 200,
  message: "Loading",
  loading: false,
  token: "",
};

interface Props {}

const errorStyle = {
  paddingLeft: "10px",
  marginBottom: "10px",
};

const Login: React.FC<Props> = () => {
  const LoginSchema = Yup.object().shape({
    UsernameLogin: Yup.string()
      .required("Required field")
      .min(6, "Username should have atleast 6 characters")
      .max(12, "Username should have maximum 12 characters"),
    PasswordLogin: Yup.string()
      .min(6, "Password should have atleast 6 characters")
      .max(20, "Password should have maximum 20 characters")
      .required("Required field"),
  });

  const RegisterSchema = Yup.object().shape({
    UsernameRegister: Yup.string()
      .required("Required field")
      .min(6, "Username should have atleast 6 characters")
      .max(12, "Username should have maximum 12 characters"),
    PasswordRegister: Yup.string()
      .min(6, "Password should have atleast 6 characters")
      .max(20, "Password should have maximum 20 characters")
      .required("Required field"),
  });

  const user: UserObject = useSelector((state: RootState) => state.user);

  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    setError(false);
  }, []);

  const handleLogin = async (values: {
    UsernameLogin: string;
    PasswordLogin: string;
  }) => {
    try {
      dispatch(loginSuccess(loadingTrue));

      const result = await LoginWithToken(
        values.UsernameLogin,
        values.PasswordLogin
      );

      if (result.status === 200 && result.token) {
        dispatch(loginSuccess(loadingFalse));

        window.localStorage.setItem("token", result.token);
        dispatch(loginSuccess(result));
        history.push(`/game`);
      } else {
        dispatch(loginSuccess(loadingFalse));
        setError(true);
        setErrorMessage(result.message);
      }
    } catch (err) {
      dispatch(loginSuccess(loadingFalse));
      setError(true);
      setErrorMessage("Unknown error. Please reload the page and try again.");
    }
  };

  const handleRegister = async (values: {
    UsernameRegister: string;
    PasswordRegister: string;
  }) => {
    try {
      dispatch(loginSuccess(loadingTrue));

      const result = await RegisterWithToken(
        values.UsernameRegister,
        values.PasswordRegister
      );

      if (result.status === 200 && result.token) {
        dispatch(loginSuccess(loadingFalse));
        window.localStorage.setItem("token", result.token);
        dispatch(registerSuccess(result));
        history.push(`/game`);
      } else {
        dispatch(loginSuccess(loadingFalse));
        setError(true);
        setErrorMessage(result.message);
      }
    } catch (err) {
      dispatch(loginSuccess(loadingFalse));
      setError(true);
      setErrorMessage("Unknown error. Please reload the page and try again.");
    }
  };

  const toggleForm = () => {
    const container = document.querySelector(".container");
    container?.classList.toggle("active");
    setError(false);
    setErrorMessage("");
  };

  return (
    <LoginStyles>
      <section>
        <div className="container">
          <Formik
            onSubmit={handleLogin}
            initialValues={{
              UsernameLogin: "",
              PasswordLogin: "",
            }}
            validationSchema={LoginSchema}
          >
            {({ handleSubmit, errors, touched }: any) => (
              <div className="user signinBx">
                <div className="imgBx">
                  <img
                    src="https://www.eu-startups.com/wp-content/uploads/2020/03/48407430_2471172019565663_1128722902946938880_n.png"
                    alt=""
                  />
                </div>
                <div className="formBx">
                  <form onSubmit={handleSubmit} className="form-vertical">
                    <h2
                      className="greenish-blue-text mt-0 mb-4 bolder"
                      style={{ color: "#363636" }}
                    >
                      <strong>
                        Hello friend, tell me your name and password...
                      </strong>
                    </h2>
                    <div className="form-group is-empty">
                      <label>Name</label>
                      <div className="group">
                        <Field
                          name="UsernameLogin"
                          className="form-control"
                          placeholder="Your name"
                          autoComplete="off"
                        />
                        {errors.UsernameLogin && touched.UsernameLogin ? (
                          <p className="alert alert-danger" style={errorStyle}>
                            {errors.UsernameLogin}
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <div className="form-group is-empty">
                      <label>Password</label>
                      <div className="group">
                        <Field
                          name="PasswordLogin"
                          className="form-control"
                          placeholder="Enter your password"
                          autoComplete="off"
                          type="password"
                        />
                        {errors.PasswordLogin && touched.PasswordLogin ? (
                          <p className="alert alert-danger" style={errorStyle}>
                            {errors.PasswordLogin}
                          </p>
                        ) : null}
                        {error ? (
                          <p className="alert alert-danger" style={errorStyle}>
                            {errorMessage}
                          </p>
                        ) : null}
                      </div>
                    </div>

                    {user?.loading ? (
                      <div style={{ textAlign: "center", opacity: 0.5 }}>
                        <input
                          type="submit"
                          value="Loading..."
                          disabled={true}
                        />
                      </div>
                    ) : (
                      <div style={{ textAlign: "center" }}>
                        <input
                          type="submit"
                          value="Let's go ➡"
                          disabled={false}
                        />
                      </div>
                    )}

                    <p className="signup">
                      Don't have an account?
                      <a href="#" onClick={toggleForm}>
                        {" "}
                        Sign Up.
                      </a>
                    </p>
                  </form>
                </div>
              </div>
            )}
          </Formik>

          <Formik
            onSubmit={handleRegister}
            initialValues={{
              UsernameRegister: "",
              PasswordRegister: "",
            }}
            validationSchema={RegisterSchema}
          >
            {({ handleSubmit, errors, touched }: any) => (
              <div className="user signupBx">
                <div className="formBx">
                  <form onSubmit={handleSubmit} className="form-vertical">
                    <h2
                      className="greenish-blue-text mt-0 mb-4 bolder"
                      style={{ color: "#363636" }}
                    >
                      <strong>Register new account</strong>
                    </h2>
                    <div className="form-group is-empty">
                      <label>Name</label>
                      <div className="group">
                        <Field
                          name="UsernameRegister"
                          className="form-control"
                          placeholder="Your name"
                          autoComplete="off"
                          helpertext={errorMessage}
                        />
                        {errors.UsernameRegister && touched.UsernameRegister ? (
                          <p className="alert alert-danger">
                            {errors.UsernameRegister}
                          </p>
                        ) : null}
                      </div>
                    </div>
                    <div className="form-group is-empty">
                      <label>Password</label>
                      <div className="group">
                        <Field
                          name="PasswordRegister"
                          className="form-control"
                          placeholder="Enter your password"
                          autoComplete="off"
                          type="password"
                          helpertext={errorMessage}
                        />
                        {errors.PasswordRegister && touched.PasswordRegister ? (
                          <p className="alert alert-danger">
                            {errors.PasswordRegister}
                          </p>
                        ) : null}
                        {error ? (
                          <p className="alert alert-danger">{errorMessage}</p>
                        ) : null}
                      </div>
                    </div>
                    {user?.loading ? (
                      <div style={{ textAlign: "center", opacity: 0.5 }}>
                        <input type="submit" value="Loading" disabled={true} />
                      </div>
                    ) : (
                      <div style={{ textAlign: "center" }}>
                        <input
                          type="submit"
                          value="Register"
                          disabled={false}
                        />
                      </div>
                    )}

                    <p className="signup">
                      Already have an account?
                      <a href="#" onClick={toggleForm}>
                        {" "}
                        Log In.
                      </a>
                    </p>
                  </form>
                </div>
                <div className="imgBx">
                  <img
                    src="https://www.eu-startups.com/wp-content/uploads/2020/03/48407430_2471172019565663_1128722902946938880_n.png"
                    alt=""
                  />
                </div>
              </div>
            )}
          </Formik>
        </div>
      </section>
    </LoginStyles>
  );
};

export default Login;
