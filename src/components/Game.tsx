import { AnimatePresence, motion } from "framer-motion";
import React, { useEffect, useState } from "react";
import { DragDropContext, DropResult } from "react-beautiful-dnd";
import { useSelector } from "react-redux";
import { Dimmer, Loader, Message, Segment } from "semantic-ui-react";
import { CORRECTFIELDS, penalty, PUZZLES } from "../custom/data";
import { InitialPositionProps, IPuzzles } from "../custom/interfaces";
import { useInterval } from "../custom/useInverval";
import { move, shuffle, ZoovuName } from "../custom/utils";
import { RootState } from "../redux/store";
import { CreateRecord } from "../services/record";
import { GameStyle } from "../styles/GameStyle";
import { UserObject } from "../types";
import Dropzone from "./Dropzone";
import DropzoneSmall from "./DropzoneSmall";
import GameModal from "./GameModal";
import Logout from "./Logout";

interface Props {}

const initialPosition = {
  start: shuffle(PUZZLES),
  singleOne: [],
  singleTwo: [],
  singleThree: [],
  singleFour: [],
  singleFive: [],
};

const animationVariant = {
  visible: { opacity: 1, y: 0 },
  hidden: { opacity: 0, y: -100 },
};

const Game: React.FC<Props> = () => {
  const user: UserObject = useSelector((state: RootState) => state.user);
  const [isRunning, setIsRunning] = useState(false);
  const [gameEnd, setGameEnd] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [timer, setTimer] = useState(30);
  const [restartGame, setRestartGame] = useState(false);
  const [showPenalty, setShowPenalty] = useState(false);
  const [position, setPosition] = useState(
    initialPosition as InitialPositionProps
  );
  const [wrongAnswer, setWrongAnswer] = useState(false);
  const positionValues = Object.values(position);

  const onDragEnd = ({ source, destination }: DropResult) => {
    if (!destination) {
      return;
    }

    const moves = move(position, source, destination);

    setPosition((prevState: any) => ({ ...prevState, ...moves }));
  };

  useEffect(() => {
    if (timer <= 0) {
      setGameEnd(true);
    }
  }, [timer]);

  //GAME TIMER
  useInterval(
    () => {
      setTimer(timer - 1);
    },
    isRunning ? 1000 : null
  );

  //OPEN/CLOSE MODALS
  const handleOpenModal = () => {
    if (gameEnd) {
      setOpenModal(true);
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  async function createRcr(username: string, time: number) {
    const record = time * 100;
    await CreateRecord(username, record);
  }

  //ON GAME END
  useEffect(() => {
    if (gameEnd && user && user.data) {
      setIsRunning(false);
      handleOpenModal();

      createRcr(user.data.username, timer);
    }
  }, [gameEnd]);

  //BUTTON SHAKE
  useEffect(() => {
    if (wrongAnswer) {
      const timer = setTimeout(() => {
        setWrongAnswer(false);
      }, 800);
      return () => clearTimeout(timer);
    }
  }, [wrongAnswer]);

  //RESTART GAME
  useEffect(() => {
    if (restartGame) {
      setGameEnd(false);
      setOpenModal(false);
      setIsRunning(false);
      setTimer(30);
      setRestartGame(false);
      setPosition({ ...{ start: shuffle(PUZZLES) }, ...initialPosition });
      setWrongAnswer(false);
    }
  }, [restartGame]);

  //CHECK FOR WINNING CONDITION
  useEffect(() => {
    if (positionValues) {
      if (checkIfCorrect()) {
        setGameEnd(true);
      }
    }

    return;
  }, [positionValues]);

  const buttonCheck = () => {
    if (checkIfCorrect()) {
      setGameEnd(true);
    } else {
      setWrongAnswer(true);
    }
  };

  const checkIfCorrect = () => {
    let zoovu = "";
    positionValues.forEach((item: IPuzzles) => {
      const field = item[0];
      if (field) zoovu = zoovu + field.letter;
    });

    if (zoovu === ZoovuName) {
      return true;
    }
  };

  const handleRestartGame = () => {
    setRestartGame(true);
  };

  const startGame = () => {
    setIsRunning(true);
    setGameEnd(false);
  };

  const handleSetPenalty = (penalty: number) => {
    setTimer((timer) => timer - penalty);
    setShowPenalty(true);
  };

  useEffect(() => {
    if (showPenalty) {
      const timer = setTimeout(() => {
        setShowPenalty(false);
      }, 1500);

      return () => clearTimeout(timer);
    }
  }, [showPenalty]);

  if (user?.data) {
    return (
      <>
        {openModal && (
          <GameModal
            isRunning={isRunning}
            open={openModal}
            handleCloseModal={handleCloseModal}
            handleOpenModal={handleOpenModal}
            score={timer}
            handleRestartGame={handleRestartGame}
          ></GameModal>
        )}
        <AnimatePresence>
          <motion.div
            style={{ margin: 0, padding: 0 }}
            key={"game"}
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opacity: 0 }}
            transition={{ duration: 0.8 }}
          >
            <GameStyle>
              <motion.div
                className="name"
                initial="hidden"
                animate="visible"
                transition={{ delay: 0.3 }}
                variants={animationVariant}
              >
                <strong> Good luck, {user?.data.username}</strong>
              </motion.div>

              <motion.div
                className="score"
                initial="hidden"
                animate="visible"
                transition={{ delay: 0.3 }}
                variants={animationVariant}
              >
                <span>
                  <AnimatePresence>
                    {showPenalty && (
                      <motion.div
                        key="penalty"
                        initial={{ opacity: 0 }}
                        animate={{ opacity: 1 }}
                        exit={{ opacity: 0 }}
                      >
                        <Message
                          color="red"
                          info
                          style={{
                            position: "absolute",
                            top: "120px",
                            right: "30px",
                          }}
                        >
                          Wrong letter placement, -{penalty} seconds
                        </Message>
                      </motion.div>
                    )}
                  </AnimatePresence>
                  <i className="clock outline icon green"></i> Time left:
                  <span style={{ paddingLeft: "5px" }}>
                    {timer} second{timer < 2 ? "" : "s"}
                  </span>
                </span>
              </motion.div>

              <motion.div
                className="textLeft"
                initial="hidden"
                animate="visible"
                transition={{ delay: 0.3 }}
                variants={animationVariant}
              >
                <p>Pick up the right cards</p>
              </motion.div>

              <div className="textCenter">
                <motion.div
                  initial="hidden"
                  animate="visible"
                  transition={{ delay: 0.3 }}
                  variants={animationVariant}
                >
                  <Logout handleRestartGame={handleRestartGame}></Logout>
                </motion.div>
              </div>

              <motion.div
                initial="hidden"
                animate="visible"
                className="textRight"
                transition={{ delay: 0.3 }}
                variants={animationVariant}
              >
                <p>The faster the better</p>
              </motion.div>
              <div className="textCenterLeft">
                <p>...and drop them here to make hte logo gret again!</p>
              </div>

              <motion.div
                initial={{ opacity: 0 }}
                animate={{ opacity: 1 }}
                exit={{ opacity: 0 }}
                transition={{ delay: 0.3 }}
                className="check"
              >
                <motion.div
                  animate={{
                    x:
                      wrongAnswer && isRunning
                        ? [-15, 15, -15, 15, -15, 15, -15, 15]
                        : 0,
                  }}
                  onClick={buttonCheck}
                  className={
                    isRunning
                      ? "massive ui red vertical animated button"
                      : "massive ui green vertical animated button"
                  }
                >
                  {!isRunning ? (
                    <>
                      <div className="visible content">
                        Move the puzzles to start te game
                      </div>
                      <div className="hidden content">Please</div>
                    </>
                  ) : (
                    <>
                      <div className="visible content">
                        {wrongAnswer && isRunning
                          ? "Keep Trying!"
                          : "Check Result"}
                        <i className="gamepad icon"></i>
                      </div>
                      <div className="hidden content">
                        {wrongAnswer && isRunning
                          ? "Keep Trying!"
                          : "Did you win?"}
                      </div>
                    </>
                  )}
                </motion.div>
              </motion.div>

              <DragDropContext
                onDragEnd={onDragEnd}
                onDragStart={() => {
                  startGame();
                }}
              >
                <div className="puzzles-start">
                  <Dropzone id={"start"} position={position.start} />
                </div>
                <div className="puzzles-end">
                  {CORRECTFIELDS.map((field) => (
                    <DropzoneSmall
                      key={field.name}
                      handleSetPenalty={handleSetPenalty}
                      id={field.name}
                      nr={field.nr}
                      position={positionValues[field.nr]}
                      letter={field.letter}
                    />
                  ))}
                </div>
              </DragDropContext>
            </GameStyle>
          </motion.div>
        </AnimatePresence>
      </>
    );
  } else {
    return (
      <Segment>
        <Dimmer active>
          <Loader size="huge">Loading</Loader>
        </Dimmer>
      </Segment>
    );
  }
};

export default Game;
