import { AnimatePresence, motion } from "framer-motion";
import React from "react";

import Login from "../components/Login";

interface Props {}

const LoginPage: React.FC<Props> = () => {
  return (
    <AnimatePresence>
      <motion.div
        style={{ margin: 0, padding: 0 }}
        key={"game"}
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opacity: 0 }}
        transition={{ duration: 1.3 }}
      >
        <Login></Login>
      </motion.div>
    </AnimatePresence>
  );
};

export default LoginPage;
