export interface ILetter {
  id: string;
  name: string;
  img: string;
  letter: string;
}

export interface DropzoneProps {
  position: any;
  id: string;
  nr: number;
  handleSetPenalty: Function;
}

export interface DropzoneSmallProps {
  position: any;
  id: string;
  nr: number;
  handleSetPenalty: Function;
  letter: string;
}

export interface LetterProps {
  name: string;
  img: string;
  id: string;
  index: number;
}

export interface IPuzzle {
  id: number;
  name: string;
  letter: string;
  img: string;
}

export interface InitialPositionProps {
  start: Array<IPuzzle>;
  singleOne: Array<IPuzzle>;
  singleTwo: Array<IPuzzle>;
  singleThree: Array<IPuzzle>;
  singleFour: Array<IPuzzle>;
  singleFive: Array<IPuzzle>;
}

export interface IPuzzles extends Array<IPuzzle> {}
