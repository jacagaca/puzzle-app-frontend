export interface UserObject {
  data?: User;
  error: boolean;
  status: number;
  message: string;
  token?: string;
  loading?: boolean;
}

export interface ActionLoading {
  loading: boolean;
}

export interface User {
  createdAt: string;
  password: string;
  role: string;
  updatedAt: string;
  username: string;
  __v: number;
  _id: string;
}
