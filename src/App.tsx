import React, { useEffect } from "react";
import { Route, Switch, BrowserRouter, Router } from "react-router-dom";
import { useDispatch } from "react-redux";
import LoginPage from "./pages/LoginPage";

import PrivateRoute from "./components/privateRoute";
import GamePage from "./pages/GamePage";
import { notFound } from "./pages/404";
import history from "./custom/history";
import { GetUser } from "./services/user";
import { loginSuccess } from "./redux/actions";
import { GlobalStyle } from "./styles/GlobalStyle";
import { ThemeProvider } from "styled-components";
import { theme } from "./styles/Theme";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    let token = window.localStorage.getItem("token");
    fetchUser(token);

    window.addEventListener("storage", () => {
      token = window.localStorage.getItem("token");
      fetchUser(token);
    });

    async function fetchUser(tok: string | null) {
      if (tok) {
        const user = await GetUser(tok);

        if (user.data) dispatch(loginSuccess(user));

        history.push("/game");
      } else {
        history.push("/");
      }
    }
  }, []);

  return (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <BrowserRouter>
          <Router history={history}>
            <Switch>
              <Route
                exact
                path={"/"}
                render={(props) => <LoginPage {...props} />}
              />
              <PrivateRoute
                exact
                path={`/game`}
                component={(props) => <GamePage {...props} />}
              />
              <Route path="*" component={notFound} />
            </Switch>
          </Router>
        </BrowserRouter>
      </ThemeProvider>
    </>
  );
}

export default App;
