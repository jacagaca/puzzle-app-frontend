import React from "react";
import { mount } from "enzyme";
import App from "../App";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

const mockStore = configureMockStore();

let store: any;

const state = {
  error: false,
  status: 200,
  message: "",
  token: "",
  loading: false,
};

beforeEach(() => {
  store = mockStore(state);
});
it("renders component and maches snap", async () => {
  const wrapper = mount(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(wrapper).toMatchSnapshot();
});
