import React from "react";
import { mount } from "enzyme";

import configureMockStore from "redux-mock-store";
import Dropzone from "../components/Dropzone";
import { PUZZLES } from "../custom/data";
import { shuffle } from "../custom/utils";

const mockStore = configureMockStore();

let store: any;

const state = {
  error: false,
  status: 200,
  message: "",
  token: "",
  loading: false,
};

beforeEach(() => {
  store = mockStore(state);
});
const position = {
  start: shuffle(PUZZLES),
};

it("renders component and maches snap", async () => {
  const wrapper = mount(<Dropzone id={"start"} position={position.start} />);

  expect(wrapper).toMatchSnapshot();
});
