import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  html, body {
    position: relative;
    min-height: 100vh;
    box-sizing: border-box;
    font-size: 10px;
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    font-family: "Poppins", sans-serif;
    background: rgb(255,255,255);
    background: linear-gradient(180deg, rgba(255,255,255,1) 0%, rgba(227,227,227,1) 100%);
}

*, *:before, *:after{
  box-sizing: inherit
}


 


 
`;
/* color: ${(props) => (props.whiteColor ? "white" : "black")}; */
