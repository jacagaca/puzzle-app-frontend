export type themeType = {
  lightBlack: string;
  black: string;
  grey: string;
  silver: string;
  purple: string;
  mediumSize: string;
  smallHeightSize: string;
};

export const theme: themeType = {
  lightBlack: "#231F20",
  black: "#20211D",
  grey: "#3A3A3A",
  silver: "#aeaeae",
  purple: "#3b0078",
  mediumSize: "1140px",
  smallHeightSize: "620px",
};
