import styled from "styled-components";

export const LoginStyles = styled.div`
  section {
    position: relative;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 20px;

    .container {
      position: relative;
      width: 800px;
      height: 500px;
      background: #fff;
      box-shadow: 0 15px 50px rgba(0, 0, 0, 0.1);
      overflow: hidden;

      .user {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: flex;

        .formBx {
          position: relative;
          width: 50%;
          height: 100%;
          background: #fff;
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 40px;
          transition: 0.5s;

          form .signup {
            text-align: center;
            position: relative;
            margin-top: 20px;
            font-size: 12px;
            letter-spacing: 1px;
            color: ${(props) => props.theme.purple};

            font-weight: 300;

            a {
              font-weight: 600;
              text-decoration: none;
              color: ${(props) => props.theme.purple};
            }
          }

          form input {
            position: relative;
            width: 100%;
            padding: 10px;
            background: #f5f5f5;
            color: ${(props) => props.theme.purple};
            border: none;
            outline: none;
            box-shadow: none;
            margin: 8px 0;
            font-size: 14px;
            letter-spacing: 1px;
            font-weight: 300;
          }

          input[type="submit"] {
            margin-top: 20px;
            width: 120px;
            background: #fff;
            color: ${(props) => props.theme.purple};
            cursor: pointer;
            font-size: 14px;
            font-weight: 700;
            letter-spacing: 1px;
            transition: 0.5s;
            padding-left: 20px;
            padding-right: 20px;

            -webkit-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 0px 3px 0px rgba(0, 0, 0, 0.75);
            border-radius: 20px;
          }

          form h2 {
            font-size: 18px;
            font-weight: 600;
            letter-spacing: 2px;
            text-align: center;
            width: 100%;
            margin-bottom: 10px;
            color: ${(props) => props.theme.purple};
          }
        }

        .imgBx {
          position: relative;
          width: 50%;
          height: 100%;
          background: #ff0;
          transition: 0.5s;

          img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
          }
        }
      }
    }
  }

  section .container .signupBx {
    pointer-events: none;
  }

  section .container.active .signupBx {
    pointer-events: initial;
  }

  section .container .signupBx .formBx {
    left: 100%;
  }

  section .container.active .signupBx .formBx {
    left: 0;
  }

  section .container .signupBx .imgBx {
    left: -100%;
  }

  section .container.active .signupBx .imgBx {
    left: 0%;
  }

  section .container .signinBx .formBx {
    left: 0%;
  }

  section .container.active .signinBx .formBx {
    left: 100%;
  }

  section .container .signinBx .imgBx {
    left: 0%;
  }

  section .container.active .signinBx .imgBx {
    left: -100%;
  }

  @media (max-width: 991px) {
    section .container {
      max-width: 400px;
    }

    section .container .imgBx {
      display: none;
    }

    section .container .user .formBx {
      width: 100%;
    }
  }
`;
