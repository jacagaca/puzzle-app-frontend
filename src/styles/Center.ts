import styled from "styled-components";

export const Center = styled.div`
  display: block;
  @media (max-width: ${(props) => props.theme.mediumSize}) {
    display: inline-block;
    width: 80%;
    text-align: center;
    position: absolute;
    transform: translate(-50%, -50%);
    top: 50%;
    left: 50%;
  }
`;
