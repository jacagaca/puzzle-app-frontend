import styled from "styled-components";

export const GameStyle = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: 30px;
  gap: 0px 0px;
  grid-template-areas:
    "name . score"
    "textLeft . textRight"
    ". . ."
    ". . ."
    ". . ."
    ". check .";
  padding: 50px;
  font-weight: 500;
  letter-spacing: 1.1px;

  @media (max-width: ${(props) => props.theme.mediumSize}) {
    padding: 20px;
    display: grid;
    grid-template-columns: 70px auto 70px;

    grid-template-areas:
      "name . score"
      "textLeft . textRight"
      ". . ."
      ". . ."
      ". . ."
      ". check .";
  }

  .name {
    font-size: 1.5rem;
    height: 50px;
    grid-area: name;
    font-weight: 900;
  }
  .score {
    font-size: 1.5rem;
    height: 50px;
    text-align: right;
    grid-area: score;
    font-weight: 600;
    color: ${(props) => props.theme.purple};
  }
  .textLeft {
    font-size: 1.1rem;
    height: 50px;
    grid-area: textLeft;
    color: ${(props) => props.theme.silver};
    margin-top: 15px;
    @media (max-width: ${(props) => props.theme.mediumSize}) {
      margin-top: 25px;
    }
  }
  .textRight {
    font-size: 1.1rem;
    height: 50px;
    text-align: right;
    grid-area: textRight;
    color: ${(props) => props.theme.silver};
    margin-top: 15px;
    @media (max-width: ${(props) => props.theme.mediumSize}) {
      margin-top: 25px;
    }
  }
  .textCenter {
    position: absolute;
    left: 50%;
    top: 60px;
    transform: translate(-50%, 50%);

    @media (max-width: ${(props) => props.theme.mediumSize}) {
      left: 50%;
    }
  }

  .check {
    text-align: center;
    grid-area: check;
    position: absolute;
    left: 50%;
    width: 100%;
    top: 550px;
    transform: translate(-50%, 50%);
  }

  .textCenterLeft {
    font-size: 1.1rem;
    padding: 30px 0 30px 0;
    grid-area: textCenterLeft;
    color: ${(props) => props.theme.silver};
    font-weight: 500;
    position: absolute;
    top: 330px;

    @media (max-width: ${(props) => props.theme.mediumSize}) {
      display: none;
    }
  }

  .puzzles-start {
    margin-top: 120px;

    @media (max-width: ${(props) => props.theme.mediumSize}) {
      display: block;
      margin: 0 auto;
      margin-top: 120px;
      height: 450px;
      margin-left: calc(50% - 90px);
    }
  }

  .puzzles-end {
    display: flex;
    margin-top: 300px;

    @media (max-width: ${(props) => props.theme.mediumSize}) {
      display: block;
      margin: 0 auto;
      margin-top: 90px;
      margin-left: calc(50% + 10px);
    }
  }

  .puzzle {
    padding: 15px;
    width: 100px;
    height: 85px;
    background-color: white;
    border: solid 5px white;
    border-radius: 5px;
    box-shadow: 0px 0px 3px 0px #f7f7f7;

    @media (max-width: ${(props) => props.theme.mediumSize}) {
      padding: 0px;
      width: 70px;
      height: 70px;
    }
  }
`;
