import styled from "styled-components";

export const EmptyContainer = styled.div`
  position: absolute;
  border: solid 1px;
  user-select: none;
  z-index: 0;
  border-radius: 5px;
  width: 110px;
  height: 100px;
  border: dashed 1.3px #21ba45;
  border-radius: 5px;
  box-shadow: 0px 0px 3px 0px #f7f7f7;
  margin-left: 18px;

  @media (max-width: ${(props) => props.theme.mediumSize}) {
    margin-left: -3px;
    width: 85px;
    height: 80px;
  }
`;
